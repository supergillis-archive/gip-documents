\documentclass[a4paper]{article}

\usepackage{tabularx}
\usepackage{framed}
\usepackage{color}
\usepackage{graphicx}
\usepackage{caption}

\def \author{Gillis Van Ginderachter}
\def \title{Talking Things: A Communication Protocol for Active RFID Technology}
\def \subtitle{Technisch document}
\def \date{7 maart 2011}

\definecolor{shadecolor}{RGB}{240, 240, 240}
\let\datatablesize\footnotesize

\newenvironment{protocol}{
	\newcommand{\req}[1]{\item[Vereiste] ##1}
	\newcommand{\msgtype}[1]{\item[Type] ##1}
	\newcommand{\desc}[1]{\item[Beschrijving] ##1}
	\newcommand{\technical}[1]{\item[Technische beschrijving] ##1}
	\newcommand{\data}[1]{\item[Voorstelling data] Zie \ref{##1} op pagina \pageref{##1}}
	\newcommand{\nodata}{\item[Voorstelling data] Dit bericht heeft geen extra data. De data in de \english{header} is bevat alle benodigde informatie.}
	\begin{shaded}\begin{description}}{\end{description}\end{shaded}\bigskip}

\newcommand{\class}[1]{\texttt{#1}}
\newcommand{\variable}[1]{\texttt{#1}}
\newcommand{\programminglanguage}[1]{\texttt{#1}}
\newcommand{\english}[1]{\textit{#1}}
\newcommand{\requirement}[1]{\texttt{#1}}
\newcommand{\library}[1]{\texttt{#1}}
\newcommand{\type}[1]{\texttt{#1}}

\newcommand{\superscript}[1]{\ensuremath{^{\textrm{#1}}}}
\newcommand{\subscript}[1]{\ensuremath{_{\textrm{#1}}}}
	
% Mix of description and itemize (description list with bullets)
\newenvironment{descriptionize}{
	\let\olditem\item
	\renewcommand\item[2][]{\olditem \textbf{##1} ##2}
	\begin{itemize}}{\end{itemize}}

\input{../tex/style}
\input{../tex/bibtex-hide-title}

\begin{document}

% Include header
\input{../tex/header}

\section*{Historiek}
\begin{tabularx}{0.8\textwidth}{|X|X|X|X|}
	\hline \textbf{Datum} & \textbf{Auteur} & \textbf{Reden} & \textbf{Beschrijving}\\
	\hline 7 maart 2011 & Gillis Van Ginderachter & Aangevraagd door de klant. & Initi\"{e}le versie.\\
	\hline
\end{tabularx}
\newpage

% Include table of contents
\input{../tex/toc}

\section{Inleiding}

\subsection{Doel}
Dit document legt het ontwerp van het te ontwikkelen systeem vast, en beschrijft hoe het systeem gerealiseerd en ge\"{i}mplementeerd zal worden.

\subsection{Kader}
Er wordt voorspeld dat in de toekomst alle fysieke dingen verbonden gaan zijn met elkaar. We krijgen dus een netwerk van zogenaamde \textit{things} die informatie met elkaar kunnen uitwisselen. Dit wordt het \textit{internet of things} genoemd.

Het doel van dit project is van dit mogelijk maken met actieve RFID tags\footnote{In dit document wordt een RFID tag kortweg tag genoemd}. Dit zijn goedkope draadloze chips die aan andere object vastgemaakt kunnen worden. De tags kunnen een gelimiteerd aantal data opslagen in hun geheugen. Door middel van een RFID node\footnote{In dit document wordt een RFID node kortweg node genoemd} kunnen we deze data uitlezen en wijzigen. Omdat de tags een batterij hebben - en dus zelf gegevens kunnen versturen - kunnen we ze extra functionaliteit geven. Zo gaan we het mogelijk maken om een bericht naar een tag of node te sturen die niet rechtstreeks in het bereik ligt van de tag of node. Dit kunnen we doen door berichten door te sturen. Dit wordt ook \textit{routing} genoemd.

\begin{figure}[ht]
	\centering \includegraphics[width=\linewidth]{hop.png}
	\caption{Dit bericht wordt doorgestuurd van Kevin naar Andy via Theo \cite{kpinte2010assignment}.}
\end{figure}

\subsection{Definities en afkortingen}
\begin{description}
	\item[RFID] Radio Frequency Identification.
		\begin{description}
			\item[Passieve tags] Dit zijn goedkope RFID tags die geen batterij hebben. Deze tags kunnen functioneren door de energie van het signaal van de node.
			\item[Actieve tags] Dit zijn iets duurdere RFID tags die een batterij hebben. Deze kunnen dus functioneren zonder node en kunnen zo zelfstandig functioneren.
		\end{description}
	\item[USB] Universal Serial Bus. Deze interface wordt gebruik om te communiceren met de RFID node.
	\item[API] Application Programming Interface.
	\item[Cross-platform] Applicaties die cross-platform zijn kunnen op verschillende operating systems werken.
	\item[Hop] Wanneer een bericht wordt doorgestuurd naar een node maakt het bericht een sprong. Deze sprong wordt een hop genoemd.
	\item[EEPROM] Electrically Erasable Programmable Read-Only Memory.
\end{description}

\subsection{Referenties}
% The following line makes sure all cites are displayed, even when not cited in this document
\nocite{*}
\bibliography{td}
\bibliographystyle{plain}

\section{Decompositie}

\subsection{Tag}
Hier worden de entiteiten die gebruikt worden in de firmware van de tags beschreven. Omdat de code van de firmware van de tags low-level code is, zijn er niet echt veel klassen, maar eerder categorie\"{e}n van functies. Deze functies zijn bijvoorbeeld functies om de nRF24L01 zender-ontvanger aan te sturen of om het EEPROM geheugen te wijzigen.

Omdat we gebruik maken van de taal \programminglanguage{C} is de encapsulatie van de entiteiten op de tag miniem. \programminglanguage{C} heeft namelijk geen klassen en dus ook geen \english{private} of \english{protected} variabelen. We kunnen alleen functies verbergen door ze niet in een \english{header} bestand te zetten. Hetzelfde geldt voor \english{information hiding}.

Sommige van de functionaliteiten zijn overgenomen van het OpenBeacon project \cite{openbeacon2010website}. Als dit het geval is, wordt het vermeld.

\subsubsection{Packet entiteit}
De \class{Packet} entiteit wordt gebruikt door het protocol om makkelijk berichten samen te stellen of uit te lezen. Zo bevat de \class{Packet} entiteit de volgende variabelen:
\begin{descriptionize}
	\item[version] Bevat het versienummer van het protocol waarmee dit bericht is samengesteld.
	\item[uid] Bevat het identificatienummer van de tag waarop dit bericht is samengesteld.
	\item[mid] Bevat het identificatienummer van het bericht. Samen met \variable{uid} vormt het een uniek identificatienummer van het bericht.
	\item[type] Bevat het type van dit bericht. Dit is nodig omdat voor elk type van bericht de \variable{data} anders ge\"{i}nterpreteerd moet worden.
	\item[hops] Bevat het aantal hops dat het bericht nog mag afleggen.
	\item[data] Bevat de data die nodig is voor het protocol om het bericht te verwerken.
\end{descriptionize}

Deze entiteit bestaat dus om aan vereiste \requirement{002.0007} te voldoen.

\subsubsection{Envelope union}
Om een instantie van een \class{Packet} makkelijk te kunnen doorsturen naar de zender-ontvanger, wordt deze in een \english{union} gezet, namelijk \class{Envelope}. In het gedetailleerd ontwerp wordt hier meer uitleg over gegeven.

\subsubsection{Zender-ontvanger hulpfuncties}
We maken gebruik van \english{macros} en hulpfuncties om de nRF24L01 zender-ontvanger aan te sturen.

Een \english{macro} is een lijst van instructies die moeten uitgevoerd worden. Zo is er zijn er de volgende \english{macros}:
\begin{descriptionize}
	\item[Init] Een \english{macro} die de nRF24L01 zender-ontvanger initialiseert.
	\item[Start] Een \english{macro} die de zender-ontvanger in \english{stand-by} modus zet.
	\item[Stop] Een \english{macro} die de zender-ontvanger in \english{power-off} modus zet.
	\item[Listen] Een \english{macro} die de zender-ontvanger in ontvanger-modus zet.
\end{descriptionize}

Er zijn ook verschillende hulpfuncties:
\begin{descriptionize}
	\item[Init] Deze functie initialiseert de nRF24L01 chip.
	\item[XcieveByte] Deze functie zet een byte over naar de nRF24L01 chip.
	\item[RegExec] Deze functie voert een registerfunctie uit.
	\item[RegWrite] Deze functie schrijft een buffer naar een bepaald register.
	\item[RegRead] Deze functie leest een bepaald register uit en schrijft het resultaat naar een buffer.
	\item[RegReadWrite] Deze functie schrijft een waarde naar een register.
	\item[Macro] Deze functie voert een \english{macro} uit.
	\item[Listen] Deze functie zet de functie in ontvanger-modus.
	\item[Execute] Deze functie begint de verzending van de \english{payload}. Werkt alleen in zender-modus.
	\item[Stop] Deze functie stop de verzending van de \english{payload}. Werkt alleen in zender-modus.
\end{descriptionize}

Deze \english{macros} en hulpfuncties werden ontwikkeld door OpenBeacon \cite{openbeacon2010website}. Enkele ervan zijn zelf toegevoegd, namelijk de \variable{Listen} hulpfunctie en de \variable{Listen} \english{macro}.

\subsubsection{EEPROM hulpfuncties}
Ook hier maken we gebruik van hulpfuncties. Zo kunnen we de toegang tot het EEPROM geheugen makkelijker maken. Het EEPROM geheugen op de tag is 256 bytes groot.

Dit zijn de verschillende hulpfuncties:
\begin{descriptionize}
	\item[write\_uint8] Schrijf een 8-\english{bits} getal naar een bepaalde positie in het EEPROM geheugen.
	\item[get\_uint8] Lees een 8-\english{bits} getal uit een bepaalde positie van het EEPROM geheugen.
	\item[write\_uint16] Schrijf een 16-\english{bits} getal naar een bepaalde positie in het EEPROM geheugen. Dit wordt opgeslagen als twee 8-\english{bit} getallen.
	\item[get\_uint16] Lees een 16-\english{bits} getal uit een bepaalde positie van het EEPROM geheugen.
	\item[write\_uint32] Schrijf een 32-\english{bits} getal naar een bepaalde positie in het EEPROM geheugen. Dit wordt opgeslagen als vier 8-\english{bit} getallen.
	\item[get\_uint32] Lees een 32-\english{bits} getal uit een bepaalde positie van het EEPROM geheugen.
	\item[write\_string] Schrijf een \english{string} naar een bepaalde positie in het EEPROM geheugen. Dit wordt opgeslagen als een sequentie van 8-\english{bit} karakters voorafgegaan van een 8-\english{bits} getal dat de lengte van de \english{string} beschrijft.
	\item[get\_string] Lees een \english{string} uit een bepaalde positie van het EEPROM geheugen.
\end{descriptionize}

Hiermee voldoen we aan de vereisten \requirement{002.0007} en \requirement{002.0009}.

\subsection{Node}
Hier worden de entiteiten die gebruikt worden in de node beschreven. De code van de firmware van de node is ook low-level code. Hier geldt hetzelfde als bij de tags met betrekking tot encapsulatie van de entiteiten en \english{information hiding}.

\subsubsection{Packet entiteit}
De \class{Packet} entiteit wordt gebruikt door het protocol om makkelijk berichten samen te stellen of uit te lezen. Deze entiteit bevat dezelfde variabelen als de \class{Packet} in de firmware van de tag omdat we hetzelfde protocol gebruiken en we berichten moeten kunnen doorsturen van een node naar een tag, en omgekeerd.

\subsubsection{Envelope union}
Om een instantie van een \class{Packet} makkelijk te kunnen doorsturen naar de zender-ontvanger, wordt deze in een \english{union} gezet, namelijk \class{Envelope}. Dit is net zoals in de firmware van de tag. In het gedetailleerd ontwerp wordt hier meer uitleg over gegeven.

\subsubsection{Zender en ontvanger}
Ook de node heeft een nRF24L01 zender-ontvanger. OpenBeacon heeft ook hier alle functies om de zender-ontvanger te besturen al ge\"{i}mplementeerd. Ook hebben ze een API ontwikkeld om de besturing nog makkelijker te maken. Die API bevat alle functies we nodig hebben om de node te programmeren.

\subsubsection{USB}
Door USB maken we de verbinding tussen de node en de API op een computer waaraan deze verbonden is. De node stuurt alle inkomende berichten door naar de USB poort die dan door de API worden uitgelezen en verwerkt. De API kan via USB commando's geven aan de node, bijvoorbeeld om een bericht de \english{broadcasten}.

De USB hulpfuncties zijn ook al door OpenBeacon geschreven. Deze hulpfuncties zijn onder andere:
\begin{descriptionize}
	\item[vUSBSendByte] Deze functie stuurt een \english{byte} naar de USB poort. Om meerdere \english{bytes} door te sturen roepen we deze functie dus meerdere keren aan.
	\item[vUSBRecvByte] Deze functie leest een buffer van een bepaalde lengte uit de USB poort.
\end{descriptionize}

Van deze functies kunnen we dus gebruik maken om communicatie via de USB poort mogelijk te maken. Hiermee is vereiste \requirement{002.0006} voldaan.

\subsection{API}
De API dient als een brug tussen de node en de programmeur. De programmeur kan dan eenvoudige functies oproepen om de node te besturen in plaats van te sukkelen met bits en bytes.

Hier worden de entiteiten die gebruikt worden in de API beschreven. De API is volledig zelf ontwikkeld en geschreven in \programminglanguage{Java} (vereiste \requirement{003.0002}) en in \programminglanguage{Python} (vereiste \requirement{003.0003}). Deze programmeertalen zijn cross-platform en we voldoen daarmee aan vereiste \requirement{003.0001}.

Hieronder worden de verschillende onderdelen van de API besproken.

\subsubsection{Reader}
Deze klasse zorgt voor toegang tot de seri\"{e}le USB poort. Het is dus een abstractie bovenop de USB poort.

Met deze klasse kunnen we dus uit de USB poort lezen en naar de USB poort schrijven. Op deze manier kunnen we communiceren met de node.

Dit zijn de functies die de klasse bevat.
\begin{descriptionize}
	\item[open] Maak de seri\"{e}le poort beschikbaar om naar te lezen en uit te schrijven.
	\item[close] Verbreek de verbinding met de seri\"{e}le poort.
	\item[write\_uint8] Schrijf een 8-\english{bits} getal naar de seri\"{e}le USB poort.
	\item[get\_uint8] Lees een 8-\english{bits} getal uit de seri\"{e}le USB poort.
	\item[write\_uint16] Schrijf een 16-\english{bits} getal naar de seri\"{e}le USB poort.
	\item[get\_uint16] Lees een 16-\english{bits} getal uit de seri\"{e}le USB poort.
	\item[write\_uint32] Schrijf een 32-\english{bits} getal naar de seri\"{e}le USB poort.
	\item[get\_uint32] Lees een 32-\english{bits} getal uit de seri\"{e}le USB poort.
	\item[write\_string] Schrijf een \english{string} naar de seri\"{e}le USB poort. Dit wordt verstuurd als een sequentie van 8-\english{bit} karakters voorafgegaan van een 8-\english{bits} getal dat de lengte van de \english{string} beschrijft.
	\item[get\_string] Lees een \english{string} uit de seri\"{e}le USB poort.
\end{descriptionize}

\subsubsection{Protocol}
De gegevens die we uitlezen uit de \class{Reader} klasse moeten ook verwerkt worden. Daarvoor dient deze \class{Protocol} klasse. Ze kent alle types van de berichten uit het protocol en kan deze dus ook verwerken en ook nieuwe berichten maken.

\subsubsection{Berichten}
Elk type bericht uit het protocol heeft een klasse die dit type voorstelt. Zo heeft de \class{TupleRequestMessage} klasse bijvoorbeeld een functie die de naam van het tupel geeft.

\subsubsection{Callbacks}
Dit systeem laat ons toe van een functie te binden aan een gebeurtenis. De \class{API} klasse maakt gebruik van dit systeem. Zo kunnen we de programmeur bijvoorbeeld verwittigen dat we een nieuw inkomend bericht hebben. Hieronder vindt u een voorbeeld in \programminglanguage{Python}.

\begin{shaded}
\begin{verbatim}
def on_inventory(self, tags):
    # do something

def on_read(self, tag, key, value):
    # do something

api.bind(API.EventInventory, on_inventory)
api.bind(API.EventRead, on_read)
\end{verbatim}
\end{shaded}

\subsubsection{API}
Deze klasse dient om de programmeur op de hoogte te houden van nieuwe berichten. Ze bevat alle functies en callbacks om de \class{Reader} klasse en de \class{Protocol} klasse aan te sturen. Ze zorgt voor de initialisatie van de \class{Reader} klasse en stuurt alle inkomende berichten door naar de \class{Protocol} klasse. Die vormt de \english{bits} en \english{bytes} dan om naar leesbare berichten, door middel van bericht klassen. Deze berichten kunnen we dan beschikbaar maken voor de programmeur aan de hand van \english{callbacks}.

De klasse bevat de volgende functies.
\begin{descriptionize}
	\item[set\_blocking] Wanneer we in de \english{blocking} staat zijn dan blokkeren we het hele programma door te wachten op berichten. Wanneer we niet in \english{blocking} staat zijn blokkeren we het programma niet en kunnen er ondertussen nog andere dingen gedaan worden (zoals een grafische gebruikersinterface).
	\item[wait\_for\_message] Deze functie wordt gebruikt wanneer we in \english{blocking} staat zijn. De functie wacht tot er een bericht beschikbaar is en geeft dit dan ook als resultaat terug.
	\item[bind] Deze functie wordt gebruikt wanneer we in niet-\english{blocking} staat zijn. Wanneer er een bericht beschikbaar is wordt er een functie opgeroepen (die door de gebruiker werd gegeven als parameter) met als parameter het nieuwe bericht.
\end{descriptionize}

\section{Afhankelijkheden}
% Deze sectie beschrijft de manier waarop de entiteiten (geïdentificeerd in de vorige sectie) aan elkaar gekoppeld zijn, en welke resources (extern aan het ontwerp) een bepaalde entiteit nodig heeft. Dit geeft een hoog-niveau beeld van hoe het systeem werkt, en stelt ons in staat de impact van vereisten- en ontwerp-veranderingen in te schatten. Verder maken deze afhankelijkheden ook duidelijk welke onderdelen eerst moeten ontwikkeld worden, en vereenvoudigen ze het lokaliseren van bepaalde errors. Principes zoals low coupling en high cohesion zijn hierbij belangrijk.
% Mogelijke afhankelijkheden zijn: entiteit 1 “gebruikt” entiteit 2 (bijv., een applicatie-component die een andere component aanroept), of entiteit 1 “heeft de aanwezigheid nodig van” entiteit 2 (bijv., een component heeft een bepaalde data-entiteit nodig).
% Resources zijn elementen die extern zijn aan het ontwerp en die nodig zijn voor de entiteit om zijn functie uit te voeren. Voorbeelden van resources zijn: fysieke toestellen (printers, enz), externe software (services), enz. De interactie-regels en methoden / strategiëen voor het gebruik van deze resource moeten hierbij opgegeven worden.

\subsection{Protocol}
Het protocol is een heel belangrijk onderdeel in dit project. Het maakt namelijk verbindingen tussen nodes, tags en computer mogelijk.

Om de impact van veranderingen in het protocol te verkleinen houdt elk bericht van het protocol een versienummer bij. Dit versienummer staat altijd in de eerste \english{byte} van het bericht. Wanneer er dan een verandering gebeurt in het protocol geven we dat protocol een nieuw versienummer en implementeren we het nieuwe protocol in de tag, node en API. Dit is veel werk, maar dat is beter dan een protocol dat niet werkt. Ook zou het helemaal niet vaak mogen voorkomen dat het protocol verandert. We hebben hier dus geen \english{low coupling}.

\subsection{Tag}

\subsubsection{Afhankelijkheden}
\begin{itemize}
	\item De tag implementeert het protocol dat is ontworpen voor dit project.
	\item De \class{Envelope} klasse gebruikt de \class{Packet} klasse om de gegevens van \class{Packet} om te zetten in \english{bytes}.
	\item De hulpfuncties van de zender-ontvanger hebben de aanwezigheid nodig van de \class{Envelope} klasse om een bericht om te zetten in \english{bytes}. Zo kunnen we makkelijk \class{Packet}en verzenden of ontvangen.
\end{itemize}

\subsection{Node}

\subsubsection{Afhankelijkheden}
\begin{itemize}
	\item De tag implementeert het protocol dat is ontworpen voor dit project.
	\item De \class{Envelope} klasse gebruikt de \class{Packet} klasse om de gegevens van \class{Packet} om te zetten in \english{bytes}.
	\item De hulpfuncties van de zender-ontvanger hebben de aanwezigheid nodig van de \class{Envelope} klasse om een bericht om te zetten in \english{bytes}. Zo kunnen we makkelijk \class{Packet}en verzenden of ontvangen.
\end{itemize}

\subsubsection{Resources}
We hebben een USB poort nodig om de node te voorzien van stroom. Deze USB poort kan een computer zijn. Zie \ref{api:resources} voor meer informatie.

\subsection{API}

\subsubsection{Afhankelijkheden}
\begin{itemize}
	\item De \class{Protocol} klasse is afhankelijk van de \class{Reader} klasse. De \class{Protocol} klasse verwerkt namelijk de berichten die de \class{Reader} klasse uitleest.
	\item De \class{Protocol} klasse gebruikt het protocol dat is ontworpen voor dit project.
	\item De \class{API} klasse is afhankelijk van de \class{Protocol} klasse en de \class{Reader} klasse. De \class{API} klasse dient om de programmeur te verwittigen als er nieuwe berichten binnenkomen, vandaar de afhankelijkheid met de \class{Reader} klasse. Maar ook om deze berichten om te zetten in bericht klassen, vandaar de afhankelijkheid met de \class{Protocol} klasse.
\end{itemize}

\subsubsection{Resources}
\label{api:resources}
We hebben een API die geschreven is in \programminglanguage{Python} en ook een API die geschreven is in \programminglanguage{Java}. We hebben dus een computer nodig waarop \programminglanguage{Python} of \programminglanguage{Java} kunnen draaien. Ook moet deze computer uitgerust zijn met een USB poort zodat we kunnen communiceren met de node.

Om te lezen en te schrijven van en naar seri\"{e}le poorten gebruikt de API gebruikt een externe \english{library}. In \programminglanguage{Python} gebruiken we daarvoor de \library{pySerial} \english{library}. In \programminglanguage{Java} is deze \english{library} standaard inbegrepen, dit is namelijk de \library{Java Communications API}.

\section{Gedetailleerd ontwerp}

\subsection{Protocol}
Om communicatie mogelijk te maken tussen tag, node en API wordt er een protocol ontwikkeld. Dit protocol is een \english{low-level} representatie van alle berichten die door de node en de tags begrepen worden. Alle gegevensuitwisseling verloopt dus volgens dit protocol. Daarmee leggen we ook een basis voor vereiste \requirement{002.0001}, dat elke node kan functioneren als een tag.

Elk bericht wordt hieronder byte per byte uitgeschrevenen er wordt ook uitgelegd wat het bericht doet. Daarmee voldoen we aan vereiste \requirement{001.0001}.

\subsubsection{Header}
\begin{protocol}
	\desc Elk bericht dat wordt verstuurd bevat een header. Deze header wordt gebruikt om de versie van het protocol, het type van het bericht, een unieke identificatie op basis van het tag identificatienummer en het node identificatienummer, het aantal hops dat het bericht nog mag afleggen en het aantal hops dat het bericht maximum mag afleggen.
	\technical Hier volgt een beschrijving van alle elementen van de header.
	\begin{descriptionize}
		\item[Versie] Wanneer we in de \english{header} meer gegevens willen bijhouden, moeten we heel ons protocol uitbreiden. We krijgen dus een nieuw protocol. Wanneer een node, die gebruik maakt van het oude protocol, wil communiceren met een tag, die gebruik maakt van het nieuwe protocol, moeten we dus weten welke versie wordt gebruikt. Anders kan er data fout uitgelezen worden. Daarom houden we dus een versienummer bij.
		\item[Type] Dit bevat het type van het bericht. Elk type bericht doet iets anders met de data, daarom dus dat we het type van het bericht moeten weten.
		\item[Unieke identificatie] De unieke identificatie van het bericht is gebaseerd op het tag identificatienummer en het bericht identificatienummer. Zo kunnen we een bericht onderscheiden van andere berichten.
		\item[Aantal hops] Dit getal bevat hoeveel hops het bericht nog mag afleggen. Wanneer het dit getal nul bereikt, wordt het niet meer verder doorgestuurd.
		\item[Maximum hops] Het kan ook zijn dat een antwoord moet teruggestuurd worden. Daarvoor hebben we dit getal nodig. Zo kunnen we wanneer we een antwoord sturen het aantal hops dat het bericht nog mag afleggen gelijkstellen aan het maximum aantal.
	\end{descriptionize}
	\data{protocol:header}
\end{protocol}

\subsubsection{Discover verzoek bericht}
\begin{protocol}
	\req \requirement{001.0012}
	\msgtype \type{12}
	\desc We versturen dit bericht als we willen weten wie er rondom ons ligt. We krijgen dan een antwoord van alle tags en nodes die rondom ons liggen.
	\technical Dit bericht bevat niet meer extra gegevens buiten de gegevens van de \english{header}. De ontvangende tags of nodes hebben niet meer gegevens nodig dan de gegevens die in de \english{header} zitten.
	\nodata
\end{protocol}

\subsubsection{Discover antwoord bericht}
\begin{protocol}
	\req \requirement{001.0013}
	\msgtype \type{13}
	\desc Dit bericht wordt verstuurd als antwoord op het \english{discover} verzoek.
	\technical Dit bericht bevat niet meer extra gegevens buiten de gegevens van de \english{header}. De ontvangende tags of nodes hebben niet meer gegevens nodig dan de gegevens die in de \english{header} zitten. Wanneer een tag of node dit bericht ontvangt weet hij dat de zender in de buurt ligt.
	\nodata
\end{protocol}

\subsubsection{Inventory verzoek bericht}
\begin{protocol}
	\req \requirement{001.0002}
	\msgtype \type{2}
	\desc Wanneer we willen weten welke nodes en tags in de buurt liggen van een bepaalde node of een bepaalde tag versturen we dit bericht. Dit bericht bevat het identificatienummer van die bepaalde node of tag.
	\data{protocol:inventory:request}
\end{protocol}

\subsubsection{Inventory antwoord bericht}
\begin{protocol}
	\req \requirement{001.0003}
	\msgtype \type{3}
	\desc Dit bericht wordt verstuurd als antwoord op het inventory verzoek. Dit bericht bevat zoveel mogelijk identificatienummers van tags en nodes in de buurt. Wanneer de identificatienummers niet passen in een bericht sturen we nog een volgend bericht met de overblijvende identificatienummers.
	\data{protocol:inventory:answer}
\end{protocol}

\subsubsection{Tupels verzoek bericht}
\begin{protocol}
	\req \requirement{001.0004}
	\msgtype \type{4}
	\desc Wanneer we willen weten welke tupels een node of een tag bevat versturen we dit bericht. Dit bericht bevat het identificatienummer van de tag.
	\data{protocol:tuples:request}
\end{protocol}

\subsubsection{Tupels antwoord bericht}
\begin{protocol}
	\req \requirement{001.0005}
	\msgtype \type{5}
	\desc Dit bericht wordt verstuurd als antwoord op het tupels verzoek. Dit bericht bevat zoveel mogelijk tupels van de tag die het bericht verstuurt. Wanneer de tupels niet passen in een bericht sturen we nog een volgend bericht met de overblijvende tupels.
	\technical 
	\data{protocol:tuples:answer}
\end{protocol}

\subsubsection{Schrijf verzoek bericht}
\begin{protocol}
	\req \requirement{001.0006}
	\msgtype \type{6}
	\desc Wanneer we een tupel naar een node of een tag willen schrijven versturen we dit bericht. Het bevat het identificatienummer en de naam van het tupel en de waarde die het tupel moet aannemen.
	\data{protocol:write:request}
\end{protocol}

\subsubsection{Lees verzoek bericht}
\begin{protocol}
	\req \requirement{001.0007}
	\msgtype \type{7}
	\desc Wanneer we een tupel van een node of een tag willen lezen versturen we dit bericht. Het bevat het identificatienummer en de naam van het tupel.
	\data{protocol:read:request}
\end{protocol}

\subsubsection{Lees antwoord bericht}
\begin{protocol}
	\req \requirement{001.0008}
	\msgtype \type{8}
	\desc Dit bericht wordt verstuurd als antwoord op het lees verzoek. Dit bericht bevat de waarde van het tupel dat uitgelezen werd.
	\data{protocol:read:answer}
\end{protocol}

\subsubsection{Verwijder verzoek bericht}
\begin{protocol}
	\req \requirement{001.0009}
	\msgtype \type{9}
	\desc Wanneer we een tupel van een node of een tag willen verwijderen versturen we dit bericht. Het bevat het identificatienummer en de naam van het tupel.
	\data{protocol:delete:request}
\end{protocol}

\subsubsection{Zoek verzoek bericht}
\begin{protocol}
	\req \requirement{001.0010}
	\msgtype \type{10}
	\desc Wanneer we in de inventory van een node of een tag een tupel willen zoeken versturen we dit bericht. Het bevat de naam van het tupel en de waarde dat het tupel moet bevatten.
	\data{protocol:search:request}
\end{protocol}

\subsubsection{Zoek antwoord bericht}
\begin{protocol}
	\req \requirement{001.0011}
	\msgtype \type{11}
	\desc Dit bericht wordt verstuurd als antwoord op het zoek verzoek. Dit bericht word verstuurd als we het tupel met de juiste waarde bevatten. In dit geval wordt dan het identificatienummer verstuurd.
	\nodata
\end{protocol}

\subsection{Tag}
\begin{center}
	\includegraphics[width=0.40\textwidth]{tag.png}
	\captionof{figure}{Tag \english{collaboration diagram}}
\end{center}

\subsubsection{Envelope}
De \class{Envelop} klasse maakt gebruik van \type{union}. In een \type{union} zitten twee of meerdere elementen. De grootte van de \type{union} is dan gelijk aan de grootte van het grootste element. De verschillende elementen maken dan gebruik van hetzelfde geheugen. Wanneer we dus een element van 4 \english{bytes} hebben en een element van 2 \english{bytes}, dan is de \type{union} 4 \english{bytes} groot. Als we dan het element van 2 \english{bytes} wijzigen wordt het element van 4 \english{bytes} ook gewijzigd.

Wij gebruiken dit om een instantie van de \class{Packet} klasse om te zetten in \english{bytes}. We maken een \type{union} van een instantie van de \class{Packet} klasse en van een \english{array} van 32 \english{bytes}. Wanneer we dan het \class{Packet} wijzigen wordt ook de \english{array} gewijzigd. Zo kunnen we dus de \english{array} meegeven aan de zender-ontvanger om een bericht te versturen, of een \english{array} omzetten naar een instantie van de \class{Packet} klasse om een bericht uit te lezen.

\subsubsection{EEPROM beheer}
We moeten een \type{integer} omzetten naar vier aparte \english{bytes} om het naar het EEPROM geheugen te schrijven. Dit doen we met behulp van \english{bitwise operators}.

Hier is een voorbeeld van hoe \type{integers} worden omgezet naar vier aparte \english{bytes}.
\begin{shaded}
\begin{verbatim}
unsigned int value;
unsigned char byte1, byte2, byte3, byte4;

// Stel dat ons integer (bestaande uit 32 bits) gelijk
// is aan 00101101 01101110 10110101 10001011

// We zetten het integer om naar unsigned char.
// Dit doen we door de eerste 8 bits van het integer
// te nemen en de rest gebruiken we niet.
// We krijgen dus 10001011
byte1 = (unsigned char)value;

// We shiften de bytes 8 keer naar links en dan
// zetten we ze om naar een unsigned char.
// We krijgen dus 00000000 0101101 01101110 10110101
// En dan 10110101
byte2 = (unsigned char)(value >> 8);

// We shiften de bytes 16 keer naar links en dan
// zetten we ze om naar een unsigned char.
// We krijgen dus 00000000 00000000 0101101 01101110
// En dan 01101110
byte3 = (unsigned char)(value >> 16);

// We shiften de bytes 24 keer naar links en dan
// zetten we ze om naar een unsigned char.
// We krijgen dus 00000000 00000000 00000000 0101101
// En dan 0101101
byte4 = (unsigned char)(value >> 24);
// We hebben dus 4 bytes die het integer voorstellen.
\end{verbatim}
\end{shaded}

\subsubsection{Routing}
\label{tag:routing}
Dit is de stategie die we gebruiken om \english{routing} mogelijk te maken. Wanneer we een bericht ontvangen wordt dit uitgelezen. Wanneer het aantal hops dat het bericht nog mag afleggen groter is dan nul wordt het terug uitgezonden. Maar voor we het uitzenden wordt wel het aantal hops dat het bericht nog mag afleggen verlaagd. 

\subsubsection{Main-loop}
\label{tag:mainloop}
De \english{main-loop} van het programma controleert constant of er nieuwe inkomende berichten zijn. Als dit het geval is dan lezen we het bericht uit en voeren we een actie uit op basis van het type van het bericht. Dit kan zijn dat we een nieuw bericht moeten verzenden als antwoord op het binnenkomend bericht.

In de main-loop zijn we constant in RX mode (zie onderstaande figuur) om berichten te kunnen ontvangen. Wanneer we dan een nieuw bericht versturen gaan we in TX mode (zie onderstaande figuur). Als het bericht verzonden is gaan we terug in RX mode en wachten we terug op binnenkomende berichten.
\begin{center}
	\includegraphics[width=\textwidth]{tranceiver.png}
	\captionof{figure}{nRF24L01 Radio \english{state diagram}}
\end{center}

\subsection{Node}

\subsubsection{Routing}
Hier wordt dezelfde strategie gebruikt als bij de tags. Zie \ref{tag:routing} voor meer informatie.

\subsubsection{Main-loop}
Hier wordt dezelfde strategie gebruikt als bij de tags. Zie \ref{tag:mainloop} voor meer informatie.

\subsection{API}
\begin{center}
	\includegraphics[width=0.75\textwidth]{api.png}
	\captionof{figure}{Tag \english{collaboration diagram}}
\end{center}

\section{Bijlage}

\subsection{Header}
\begin{shaded}
	\datatablesize
	\label{protocol:header}
	\subsubsection*{Representatie}
	\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline \textbf{Byte} & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7 & 8 & 9 & 10 & 11\\
		\hline \textbf{Beschrijving} & A & B & \multicolumn{4}{c|}{C} & \multicolumn{4}{c|}{D} & E & F\\
		\hline
	\end{tabular}
	\subsubsection*{Legende}
	\begin{descriptionize}
		\item[A] 1 \english{byte} die de versie van het protocol bevat.
		\item[B] 1 \english{byte} die het type van het bericht bevat.
		\item[C] 4 \english{bytes} die het identificatienummer van de tag waarop dit bericht is samengesteld bevatten.
		\item[D] 4 \english{bytes} die het identificatienummer van het bericht bevatten.
		\item[E] 1 \english{byte} die het aantal hops bevat dat het bericht nog mag afleggen.
		\item[F] 1 \english{byte} die het maximum aantal hops bevat dat het bericht mag afleggen.
	\end{descriptionize}
\end{shaded}

\subsection{Inventory verzoek}
\begin{shaded}
	\datatablesize
	\label{protocol:inventory:request}
	\subsubsection*{Representatie}
	\begin{tabular}{|l|c|c|c|c|}
		\hline \textbf{Byte} & 12 & 13 & 14 & 15\\
		\hline \textbf{Beschrijving} & \multicolumn{4}{c|}{A}\\
		\hline
	\end{tabular}
	\subsubsection*{Legende}
	\begin{descriptionize}
		\item[A] 4 \english{bytes} die het identificatienummer bevatten van de tag waarvan we de inventory willen weten.
	\end{descriptionize}
\end{shaded}

\subsection{Inventory antwoord}
\begin{shaded}
	\datatablesize
	\label{protocol:inventory:answer}
	\subsubsection*{Representatie}
	\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline \textbf{Byte} & 12 & 13 & 14 & 15 & 16 & 17 & 18 & 19 & 20 & 21 & 22 & 23 & ...\\
		\hline \textbf{Beschrijving} & \multicolumn{4}{c|}{A\subscript{1}} & \multicolumn{4}{c|}{A\subscript{2}} & \multicolumn{4}{c|}{A\subscript{3}} & ...\\
		\hline
	\end{tabular}
	\subsubsection*{Legende}
	\begin{descriptionize}
		\item[A] 4 \english{bytes} die het identificatienummer bevatten van de eerste tag die in de buurt ligt van de verzender.
	\end{descriptionize}
\end{shaded}

\subsection{Tupels verzoek}
\begin{shaded}
	\datatablesize
	\label{protocol:tuples:request}
	\subsubsection*{Representatie}
	\begin{tabular}{|l|c|c|c|c|}
		\hline \textbf{Byte} & 12 & 13 & 14 & 15\\
		\hline \textbf{Beschrijving} & \multicolumn{4}{c|}{A}\\
		\hline
	\end{tabular}
	\subsubsection*{Legende}
	\begin{descriptionize}
		\item[A] 4 \english{bytes} die het identificatienummer bevatten van de tag waarvan we de tupels willen weten.
	\end{descriptionize}
\end{shaded}

\subsection{Tupels antwoord}
\begin{shaded}
	\datatablesize
	\label{protocol:tuples:answer}
	\subsubsection*{Representatie}
	\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline \textbf{Byte} & 12 & 13 & ... & 13+A\subscript{1}-1 & 13+A\subscript{1} & 13+A\subscript{1}+1 & ... & 13+A\subscript{1}+A\subscript{2}-1 & ...\\
		\hline \textbf{Beschrijving} & A\subscript{1} & \multicolumn{3}{c|}{B\subscript{1}} & A\subscript{2} & \multicolumn{3}{c|}{B\subscript{2}} & ...\\
		\hline
	\end{tabular}
	\subsubsection*{Legende}
	\begin{descriptionize}
		\item[A] 1 \english{byte} die de lengte van de tupelnaam bevat.
		\item[B] A \english{bytes} die de tupelnaam bevatten.
	\end{descriptionize}
\end{shaded}

\subsection{Schrijf verzoek}
\begin{shaded}
	\datatablesize
	\label{protocol:write:request}
	\subsubsection*{Representatie}
	\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline \textbf{Byte} & 12 & 13 & 14 & 15 & 16 & 17 & ... & 17+B-1 & 17+B & 17+B+1 & ... & 17+B+D-1\\
		\hline \textbf{Beschrijving} & \multicolumn{4}{c|}{A} & B & \multicolumn{3}{c|}{C} & D & \multicolumn{3}{c|}{E}\\
		\hline
	\end{tabular}
	\subsubsection*{Legende}
	\begin{descriptionize}
		\item[A] 4 \english{bytes} die het identificatienummer bevatten van de tag waarvan we het tupel willen wijzigen.
		\item[B] 1 \english{byte} die de lengte van de tupelnaam bevat.
		\item[C] B \english{bytes} die de tupelnaam bevatten.
		\item[D] 1 \english{byte} die de lengte van de tupelwaarde bevat.
		\item[E] B \english{bytes} die de tupelwaarde bevatten.
	\end{descriptionize}
\end{shaded}

\subsection{Lees verzoek}
\begin{shaded}
	\datatablesize
	\label{protocol:read:request}
	\subsubsection*{Representatie}
	\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline \textbf{Byte} & 12 & 13 & 14 & 15 & 16 & 17 & ... & 17+B-1\\
		\hline \textbf{Beschrijving} & \multicolumn{4}{c|}{A} & B & \multicolumn{3}{c|}{C}\\
		\hline
	\end{tabular}
	\subsubsection*{Legende}
	\begin{descriptionize}
		\item[A] 4 \english{bytes} die het identificatienummer bevatten van de tag waarvan we het tupel willen lezen.
		\item[B] 1 \english{byte} die de lengte van de tupelnaam bevat.
		\item[C] B \english{bytes} die de tupelnaam bevatten.
	\end{descriptionize}
\end{shaded}

\subsection{Lees antwoord}
\begin{shaded}
	\datatablesize
	\label{protocol:read:answer}
	\subsubsection*{Representatie}
	\begin{tabular}{|l|c|c|c|c|}
		\hline \textbf{Byte} & 12 & 13 & ... & 13+A-1\\
		\hline \textbf{Beschrijving} & A & \multicolumn{3}{c|}{B}\\
		\hline
	\end{tabular}
	\subsubsection*{Legende}
	\begin{descriptionize}
		\item[A] 1 \english{byte} die de lengte van de tupelnaam bevat.
		\item[B] A \english{bytes} die de tupelnaam bevatten.
	\end{descriptionize}
\end{shaded}

\subsection{Verwijder verzoek}
\begin{shaded}
	\datatablesize
	\label{protocol:delete:request}
	\subsubsection*{Representatie}
	\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline \textbf{Byte} & 12 & 13 & 14 & 15 & 16 & 17 & ... & 17+B-1\\
		\hline \textbf{Beschrijving} & \multicolumn{4}{c|}{A} & B & \multicolumn{3}{c|}{C}\\
		\hline
	\end{tabular}
	\subsubsection*{Legende}
	\begin{descriptionize}
		\item[A] 4 \english{bytes} die het identificatienummer bevatten van de tag waarvan we het tupel willen verwijderen.
		\item[B] 1 \english{byte} die de lengte van de tupelnaam bevat.
		\item[C] B \english{bytes} die de tupelnaam bevatten.
	\end{descriptionize}
\end{shaded}

\subsection{Zoek verzoek}
\begin{shaded}
	\datatablesize
	\label{protocol:search:request}
	\subsubsection*{Representatie}
	\begin{tabular}{|l|c|c|c|c|c|c|c|c|c|c|c|c|}
		\hline \textbf{Byte} & 12 & 13 & 14 & 15 & 16 & 17 & ... & 17+B-1 & 17+B & 17+B+1 & ... & 17+B+D-1\\
		\hline \textbf{Beschrijving} & \multicolumn{4}{c|}{A} & B & \multicolumn{3}{c|}{C} & D & \multicolumn{3}{c|}{E}\\
		\hline
	\end{tabular}
	\subsubsection*{Legende}
	\begin{descriptionize}
		\item[A] 4 \english{bytes} die het identificatienummer bevatten van de tag waarin we het tupel willen zoeken.
		\item[B] 1 \english{byte} die de lengte van de tupelnaam bevat.
		\item[C] B \english{bytes} die de tupelnaam bevatten.
		\item[D] 1 \english{byte} die de lengte van de gezochte tupelwaarde bevat.
		\item[E] B \english{bytes} die de gezochte tupelwaarde bevatten.
	\end{descriptionize}
\end{shaded}

\end{document}

