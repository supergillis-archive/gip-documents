\documentclass[a4paper]{article}

\def \author{Gillis Van Ginderachter}
\def \title{Talking Things: A Communication Protocol for Active RFID Technology}
\def \subtitle{Codedocument}
\def \date{23 mei 2011}

\usepackage{courier}
\usepackage{listings}
\usepackage{color}
\usepackage{framed}
\usepackage[
	pdfauthor={\author},
	pdftitle={\title},
	pdfsubject={\subtitle},
	pagebackref=true,
	pdftex]{hyperref}

\definecolor{shadecolor}{RGB}{240, 240, 240}

\lstset{
	basicstyle=\ttfamily,
	identifierstyle=\ttfamily,
	numbers=left,
	numberstyle=\tiny,
	numbersep=5pt,
	tabsize=2,
	extendedchars=true,
	breaklines=true,
	showspaces=false,
	showtabs=false,
	framexleftmargin=2pt,
	backgroundcolor=\color{shadecolor},
	keywordstyle=\color[rgb]{0,0,1},
	commentstyle=\color[rgb]{0.133,0.545,0.133},
	stringstyle=\color[rgb]{0.627,0.126,0.941},
}

\newcommand{\tagpath}{../../codebase/tag/src/}
\newcommand{\nodepath}{../../codebase/node/gip/application/}
\newcommand{\apipath}{../../codebase/api-python/}

\newcommand{\includec}[2]{\lstinputlisting[language=C]{#1#2}}
\newcommand{\includepython}[2]{\lstinputlisting[language=Python]{#1#2}}

\newcommand{\class}[1]{\texttt{#1}}
\newcommand{\variable}[1]{\texttt{#1}}
\newcommand{\programminglanguage}[1]{\texttt{#1}}
\newcommand{\english}[1]{\textit{#1}}

\newcommand{\superscript}[1]{\ensuremath{^{\textrm{#1}}}}
\newcommand{\subscript}[1]{\ensuremath{_{\textrm{#1}}}}

\newenvironment{descriptionize}{
	\let\olditem\item
	\renewcommand\item[2][]{\olditem \textbf{##1} ##2}
	\begin{itemize}}{\end{itemize}}
	
% Hide bibtex title
\makeatletter
\renewenvironment{thebibliography}[1]
     {\@mkboth{\MakeUppercase\bibname}{\MakeUppercase\bibname}%
      \list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}
\makeatother

% Include style
\input{../tex/style.tex}

\begin{document}

% Include header
\input{../tex/header.tex}
\input{../tex/toc.tex}

\section{Inleiding}

\subsection{Doel}
Dit is het codedocument van het ge\"{i}ntegreerd project. Met dit document wordt de definitieve code ingeleverd en wordt die code beknopt uitgelegd. Ook worden de gebruikte libraries beschreven.

\subsection{Definities en afkortingen}
\begin{description}
	\item[RFID] Radio Frequency Identification.
		\begin{description}
			\item[Passieve tags] Dit zijn goedkope RFID tags die geen batterij hebben. Deze tags kunnen functioneren door de energie van het signaal van de node.
			\item[Actieve tags] Dit zijn iets duurdere RFID tags die een batterij hebben. Deze kunnen dus functioneren zonder node en kunnen zo zelfstandig functioneren.
		\end{description}
	\item[USB] Universal Serial Bus. Deze interface wordt gebruik om te communiceren met de RFID node.
	\item[API] Application Programming Interface.
	\item[EEPROM] Electrically Erasable Programmable Read-Only Memory.
\end{description}

\subsection{Referenties}
% The following line makes sure all cites are displayed, even when not cited in this document
\nocite{*}
\bibliography{code}
\bibliographystyle{plain}

\section{Decompostitie}
We kunnen de code opsplitsen in drie onderdelen, nl. de code van de \english{tag}, de code van de \english{node} en de code van de \english{API}.

\subsection{Tag}
Omdat we gebruik maken van de programmeertaal \programminglanguage{C} hebben we niet echt klassen, modules of \english{packages}. Om toch het gevoel te krijgen van \english{packages} zijn functies gegroepeerd per categorie en in een corresponderend \english{source} bestand geplaatst. Deze functies zijn bijvoorbeeld functies om de nRF24L01 zender-ontvanger aan te sturen of om het EEPROM geheugen te wijzigen.

\subsubsection{Packet}
De \class{Packet} entiteit wordt gebruikt door het protocol om makkelijk berichten samen te stellen of uit te lezen. Zo bevat de \class{Packet} entiteit de volgende variabelen:
\begin{descriptionize}
	\item[version] Bevat het versienummer van het protocol waarmee dit bericht is samengesteld.
	\item[type] Bevat het type van dit bericht. Dit is nodig omdat voor elk type van bericht de \variable{data} anders ge\"{i}nterpreteerd moet worden.
	\item[tid] Bevat het identificatienummer van de tag waarop dit bericht is samengesteld.
	\item[mid] Bevat het identificatienummer van het bericht. Samen met \variable{tid} vormt het een uniek identificatienummer van het bericht.
	\item[hop\_count] Bevat het aantal hops dat het bericht maximaal mag afleggen.
	\item[hop\_max] Bevat het aantal hops dat het bericht nog mag afleggen.
	\item[data] Bevat de data die nodig is voor het protocol om het bericht te verwerken.
\end{descriptionize}

We kunnen dan bijvoorbeeld het volgende doen:
\begin{lstlisting}[language=C]
Packet packet;
packet.version = 0;
packet.tid = tid;
packet.mid = mid++;
packet.type = PACKET_READ_REQUEST;
packet.hop_count = 5;
packet.hop_max = 5;
packet.data = "name";

send(&packet);
\end{lstlisting}

\subsubsection{Envelope}
Om een instantie van \class{Packet} makkelijk te kunnen doorsturen naar de zender-ontvanger, wordt deze samen met een \english{array} van 8-bit \english{unsigned integers} in een \english{union}, genaamd \class{Envelope}, gezet.

\subsubsection{Geheugenbeheer}
Om het geheugen en de tupels op het geheugen te beheren zijn er verschillende functies en macro's. Met deze functies kunnen we bytes uit het geheugen lezen en bytes naar het geheugen schrijven. Maar we kunnen ook tupels in het geheugen toevoegen.

De belangrijkste functies zijn degene om rechtstreeks naar het geheugen te schrijven, zoals \variable{memory\_read\_uint8}, \variable{memory\_write\_uint8}, \variable{memory\_read\_array}, \variable{memory\_write\_array}. Andere belangrijke functies zijn dan degene die de tupels beheren, zoals \variable{memory\_add\_tuple}, \variable{memory\_find\_tuple}, \variable{memory\_match\_tuple}, \variable{memory\_delete\_tuple}, \variable{memory\_crunch}.

%Hier volgt een overzicht van alle functies.
%\begin{descriptionize}
%	\item[memory\_read\_uint8] Lees een 8-bit \english{unsigned integer} uit het EEPROM geheugen.
%	\item[memory\_write\_uint8] Schrijf een 8-bit \english{unsigned integer} naar het EEPROM geheugen.
%	\item[memory\_read\_array] Lees een \english{array} van 8-bit \english{unsigned integers} uit het EEPROM geheugen.
%	\item[memory\_write\_array] Schrijf een \english{array} van 8-bit \english{unsigned integers} naar het EEPROM geheugen.
%	\item[memory\_cmp] Vergelijk een \english{array} uit het programma geheugen met een english{array} uit het EEPROM geheugen.
%	\item[memory\_copy] Kopieer een \english{array} van de ene positie uit het EEPROM geheugen naar een andere positie.
%	\item[memory\_add\_tuple] Schrijf een tupel naar het EEPROM geheugen.
%	\item[memory\_find\_tuple] Zoek een tupel in het EEPROM geheugen.
%	\item[memory\_match\_tuple] Zoek een tupel met een bepaalde waarde in het EEPROM geheugen.
%	\item[memory\_delete\_tuple] Verwijder een tupel uit het EEPROM geheugen.
%	\item[memory\_crunch] Voer een \english{crunch} uit op het EEPROM geheugen om plaats te maken in het EEPROM geheugen.
%\end{descriptionize}

\subsubsection{Protocol}
Deze entiteit bestaat eigenlijk uit een enkele functie, namelijk de \variable{protocol\_parse} functie. Toch is deze functie het belangrijkste onderdeel van de \english{tag}. Ze implementeert namelijk het protocol van dit project en zorgt dus voor de communicatie tussen \english{nodes} en andere \english{tags}.

\subsection{Node}
Omdat we gebruik maken van de programmeertaal \programminglanguage{C} hebben we - net zoals bij de \english{tags} - niet echt klassen, modules of \english{packages}. Ook hier zijn functies gegroepeerd per categorie en in een corresponderend \english{source} bestand geplaatst.

De node dient eigenlijk alleen om data te verzenden en data te ontvangen. Ze heeft zelf geen functionaliteit, ze wordt volledig bestuurd door de \english{API} via \english{USB}. Daarom zijn er dus ook niet zoveel entiteiten als bij de \english{tag}.

\subsubsection{USB}
Ook al bestaat deze entiteit maar uit twee functies, \variable{usb\_write\_data} en \variable{usb\_read\_data}, ze is de belangrijkste entiteit in de code van de \english{node}. Deze entiteit maakt communicatie met de \english{API} mogelijk.

\subsubsection{Protocol}
In deze entiteit wordt het ontvangen en doorsturen van data en de communicatie via \english{USB} geregeld. Wanneer we data ontvangen wordt de data op de \english{USB} buffer geplaatst. De \english{API} kan dan de data verwerken.

De \english{API} kan ook data verzenden via de \english{node} door data naar de \english{USB} buffer te schrijven.

\subsection{API}
De \english{API} is volledig geschreven in \programminglanguage{Python}. Deze dient als abstractielaag bovenop het protocol en de \english{node}. We kunnen met de \english{API} gemakkelijk berichten samenstellen en verzenden, maar ook gemakkelijk berichten ontvangen en uitlezen. In de code vindt u een terminal applicatie (zie pagina \pageref{code:terminal.py}) die gebruik maakt van de \english{API} om te demonstreren hoe deze concreet werkt.

\subsubsection{Buffer klasse}
Deze klasse is een klasse om makkelijk \class{bytearrays} te manipuleren. Zo zijn er hulpfuncties om 8-bit, 16-bit en 32-bit \english{unsigned integers} en \english{strings} naar de \class{bytearray} te schrijven. De klasse wordt ook gebruikt door de \class{Serializable} klasse.

\subsubsection{Serializable klasse}
Deze klasse is ook een klasse om het leven makkelijker te maken. De \class{Message} klasses implementeren de abstracte functies van deze klasse. Op deze manier kunnen \class{Messages} makkelijk naar een \class{Buffer} geschreven worden.

\subsubsection{Node klasse}
Alle communicatie met de \english{node} gebeurt via deze klasse. Deze klasse opent namelijk de \english{USB} poort en laat ons toe om \class{Buffers} naar de \english{node} te schrijven en om \class{Buffers} uit de \english{node} uit te lezen.

\subsubsection{Message klasses}
Voor elk type bericht, beschreven in het protocol, is een specifieke klasse. Zo zijn er bijvoorbeeld de klassen \class{InventoryRequestMessage}, \class{InventoryAnswerMessage}, \class{TuplesRequestMessage}, etc. Elk van deze klassen is een subklasse van de \class{Serializable} klasse. Op deze manier kunnen \class{Messages} makkelijk naar een \class{Buffer} geschreven worden en bij gevolg ook makkelijk naar de \english{node} geschreven worden.

\subsubsection{API klasse}
Deze klasse zorgt voor het ontvangen en versturen van berichten met behulp van twee \english{queues}. Wanneer we een bericht willen versturen zetten we dit op de \english{output queue}. Als we willen controleren of er een bericht ontvangen is dan controleren we of er een bericht op de \english{input queue} staat.

\section{Gebruikte libraries}

\subsection{OpenBeacon}
Het hele project is gebaseerd op broncode van het OpenBeacon\cite{openbeaconwebsite} project. Dit was door de klant zelf voorgesteld.

Veel van de code het OpenBeacon project is hergebruikt. Het spreekt voor zich dat deze code niet in dit document wordt bijgevoegd.

\subsection{HI-TECH C Compiler}
De code van de \english{tag} is gecompileerd door de \english{Lite} versie van de HI-TECH C\superscript{\textregistered} Compiler\cite{hitechwebsite}.

\subsection{PICkit 2 Command Line Interface}
Om de gecompileerde code van de \english{tag} te flashen hebben we gebruikt gemaakt van de PICkit 2 Command Line Interface.

\subsection{ARM toolchain}
De code van de \english{node} is gecompileerd met behulp van een ARM toolchain, nl. summon-arm-toolchain\cite{summonarmtoolchainwebsite}.

\subsection{AT91SAM7}
Om de gecompileerde code van de \english{tag} te flashen hebben we gebruikt gemaakt van AT91SAM7.

\section{Code}
Hieronder vindt u de broncode, opgesplitst per onderdeel en alfabetisch gesorteerd op de naam van het bestand.

\subsection{Tag}
   
\subsubsection{buffer.h}
\includec{\tagpath}{buffer.h}
   
\subsubsection{config.h}
\includec{\tagpath}{config.h}
   
\subsubsection{definitions.h}
\includec{\tagpath}{definitions.h}

\subsubsection{main.c}
\includec{\tagpath}{main.c}
   
\subsubsection{memory.c}
\includec{\tagpath}{memory.c}

\subsubsection{memory.h}
\includec{\tagpath}{memory.h}

\subsubsection{nRFCMD.c}
\includec{\tagpath}{nRFCMD.c}

\subsubsection{nRFCMD.h}
\includec{\tagpath}{nRFCMD.h}
   
\subsubsection{protocol.c}
\includec{\tagpath}{protocol.c}
   
\subsubsection{protocol.h}
\includec{\tagpath}{protocol.h}

\subsection{Node}

\subsubsection{main.c}
\includec{\nodepath}{main.c}
   
\subsubsection{protocol.c}
\includec{\nodepath}{protocol.c}
   
\subsubsection{protocol.h}
\includec{\nodepath}{protocol.h}
   
\subsubsection{usb.h}
\includec{\nodepath}{usb.h}

\subsection{API}
   
\subsubsection{api.py}
\includepython{\apipath}{api.py}
   
\subsubsection{buffer.py}
\includepython{\apipath}{buffer.py}
   
\subsubsection{node.py}
\includepython{\apipath}{node.py}
   
\subsubsection{packet.py}
\includepython{\apipath}{packet.py}
   
\subsubsection{protocol.py}
\includepython{\apipath}{protocol.py}
   
\subsubsection{serializable.py}
\includepython{\apipath}{serializable.py}
   
\subsubsection{terminal.py}
\label{code:terminal.py}
\includepython{\apipath}{terminal.py}

\end{document}
